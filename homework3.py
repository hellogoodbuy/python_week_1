import cv2

img_path = r'E:\ai\lena.jpg'

img = cv2.imread(img_path)

cv2.imshow('Orignal', img)

#使用cvtColor转换为HSV图
out_img_HSV=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)#将图片转换为灰度图
hsvChannels=cv2.split(out_img_HSV)  #将HSV格式的图片分解为3个通道

cv2.namedWindow("Hue")   #创建一个窗口
cv2.imshow('Hue',hsvChannels[0]) #显示Hue分量
cv2.namedWindow("Saturation")   #创建一个窗口
cv2.imshow('Saturation',hsvChannels[1]) #显示Saturation分量
cv2.namedWindow("Value")   #创建一个窗口
cv2.imshow('Value',hsvChannels[2]) #显示Value分量

cv2.waitKey(0)





